let $list, $input, liTitle;
var buttonAdd, listElement, addElement,deleteButton,xPopUp, doneButton, idTitle,mark ;
let delTitle;
var valStatus;

function main (){
    searchForElement();
    domeEvents();
    getTodos();
}   

function searchForElement(){
    $list = document.getElementById('list');
    buttonAdd = document.getElementById('btn-add');
    listElement = document.getElementById('list-element');
    $input= document.getElementById('txt-input');
    btnText = document.getElementById("btn-dele");
    deleteButton = document.getElementsByClassName("delete"); 
    popUp = document.getElementById("myModal"); 
    xPopUp = document.getElementById("closePopup"); 
    doneButton = document.getElementById("btn_done"); 
    cancelButton = document.getElementById("btn_cancel");
}

function domeEvents(){
    buttonAdd.addEventListener('click',addNewTodo);
    list.addEventListener('click',listClickManager);
    xPopUp.addEventListener('click',closePopUp);
    doneButton.addEventListener('click',saveValue);
    cancelButton.addEventListener('click',closePopUp); 
}

async function getTodos() {
    let todos = await axios.get('http://195.181.210.249:3000/todo/' );
    todos.data.forEach(dos =>{
                if(dos.author == "AG") addNewElementToList(dos.title,dos.id, dos.extra);
     })
} 

 function addNewElementToList(title, id,extra){
        const newElement = createElement(title);
        newElement.id=id;

        if(extra =="done"){
            newElement.style.color="white";
            newElement.style.backgroundColor="grey";
            newElement.style.textDecoration ="line-through";
        }

        let newButton = document.createElement('button');
        newButton.innerText = "Delete";
        newButton.id=id;
        newButton.className="delete";
        
        let newButton2 = document.createElement('button');
        newButton2.innerText = "Edit";
        newButton2.id=id;
        newButton2.className="edit";
        
        let newButton3 = document.createElement('button');
        newButton3.innerText = "Mark as done"; 
        newButton3.id=id;
        newButton3.className="mark";
    
        newElement.appendChild(newButton);
        newElement.appendChild(newButton2);
        newElement.appendChild(newButton3);
        $list.appendChild(newElement);
}

function createElement(title){
    var newElement = document.createElement('li');
    newElement.innerText = title;
    newElement.id=('text');
    return newElement;
} 

function clearList(){
// location.reload();
$list.innerHTML = '';
getTodos();
}

async function addNewTodo() {
     await axios.post('http://195.181.210.249:3000/todo/', {
        title: $input.value,
        author: 'AG',
        extra: 'active'
    }); 
    clearList();   
}

async function deleteTodo() {
    await axios.delete('http://195.181.210.249:3000/todo/' + delTitle,);
}

async function markTodo() {
    anakonda = await axios.put('http://195.181.210.249:3000/todo/' + idMark,{
        extra: 'done'
    });  
}

async function markDownTodo() {
    await axios.put('http://195.181.210.249:3000/todo/' + idMark,{
        extra: 'active'
    });   
} 

function listClickManager(eventObject){
    if(eventObject.target.className === 'delete') {
        delTitle = eventObject.target.parentElement.id;   
        eventObject.target.parentElement.remove("li");
        deleteTodo();    
    }

    else if(eventObject.target.className === 'mark'){
        eventObject.target.className= "mark-checked";
        idMark = eventObject.target.parentElement.id;
        mark = eventObject.target.parentElement;
        mark.style.textDecoration ="line-through";
        mark.style.backgroundColor="grey";
        mark.style.color="white";
        markTodo();
        }

    else if (eventObject.target.className === 'mark-checked'){
        eventObject.target.className= "mark";
        idMark = eventObject.target.parentElement.id;
        mark = eventObject.target.parentElement;
        mark.style.textDecoration ="none";
        mark.style.backgroundColor="white";
        mark.style.color="black";
        markDownTodo();
    }
        
    else if (eventObject.target.className === 'edit'){
        popUp.style.display="block";
        let editTodoTitle = eventObject.target.parentNode.firstChild.nodeValue;
        idTitle = eventObject.target.parentElement.id;
        document.getElementById('popupInput').value = editTodoTitle;     
    }   
}

function closePopUp(){
    popUp.style.display="none";
}

async function saveValue(){
    editTodoTitle = document.getElementById('popupInput').value;
    document.getElementById(idTitle).firstChild.nodeValue = editTodoTitle;
    popUp.style.display="none";
    await axios.put('http://195.181.210.249:3000/todo/' + idTitle,{
        title: editTodoTitle, 
    }); 
} 

document.addEventListener('DOMContentLoaded', main);

